# REGISTRATION EVENTS

## INTRODUCTION

This module provides a very simple API for generating and dispatching events when a registration token is present during user registration. This module has no UI and is only intended for use with other contrib modules.
## INSTALLATION

* Install the module as per: [https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules](https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules)

## USAGE

### Token generation

To create a token, all you need to do is create a RegistrationEvent entity:

```php
$entity = RegistrationEvent::create(
    [
        'registration_key' => 'UNIQUE_KEY_DESCRIBING_EVENT',
        'registration_data' => [],
    ]
);
$entity->save();

$url = $entity->getRegistrationUrl();
```

Any data you wish to pass to the registration event can be put into 'registration_data'.

You can then send the registration link via email or whatever means needed.

Lastly, create an event subscriber that listens for the registration_key described above:

```php
namespace Drupal\example_module\EventSubscriber;

use Drupal\registration_events\Event\RegistrationEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ExampleModuleSubscriber implements EventSubscriberInterface
{
    /**
     * @param RegistrationEvent $event
     */
    public function onRegistration(RegistrationEvent $event)
    {
        $registrationKey = $event->getRegistrationKey();
        $registrationData = $event->getRegistrationData();
        $account = $event->getAccount();

        \\ Do something here with registration data...
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            'UNIQUE_KEY_DESCRIBING_EVENT' => 'onRegistration',
        ];
    }
}

```

When a user registers using a url containing the registration token, the event will be dispatched and you can set roles whatever fits your use case.
