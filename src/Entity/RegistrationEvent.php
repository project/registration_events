<?php

namespace Drupal\registration_events\Entity;

use Drupal;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Url;
use Exception;

/**
 * @ContentEntityType(
 *   id = "registration_event",
 *   label = @Translation("Registration Event"),
 *   base_table = "registration_event",
 *   fieldable = FALSE,
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *   },
 * )
 */
class RegistrationEvent extends ContentEntityBase implements ContentEntityInterface
{
    use EntityChangedTrait;

    /**
     * {@inheritdoc}
     */
    public static function baseFieldDefinitions(EntityTypeInterface $entity_type)
    {
        $fields = parent::baseFieldDefinitions($entity_type);

        $fields['registration_token'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Registration Token'))
            ->setSettings(['max_length' => 512, 'text_processing' => 0,]);

        $fields['registration_key'] = BaseFieldDefinition::create('string')
            ->setLabel(t('Registration key'))
            ->setSettings(['max_length' => 512, 'text_processing' => 0,]);

        $fields['registration_data'] = BaseFieldDefinition::create('map')
            ->setLabel(t('Registration data'));

        $fields['created'] = BaseFieldDefinition::create('created')
            ->setLabel(t('Created'));

        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public static function preCreate(EntityStorageInterface $storage, array &$values)
    {
        if (!isset($values['registration_token'])) {
            try {
                $values['registration_token'] = $token = bin2hex(random_bytes(32));
            }
            catch (Exception $e) {
                Drupal::logger('registration_events')->error('Error generating registration token.');
            }
        }
    }

    public function getRegistrationUrl (): string {
        $token = $this->get('registration_token')->getValue();

        if (!isset($token[0]['value']) || empty($token[0]['value'])) throw new Exception('Unable to get registration token.');

        return Url::fromRoute(
            'user.register',
            [REGISTRATION_EVENTS_PARAMETER => $token[0]['value']],
            ['absolute' => true]
        )->toString();
    }
}
