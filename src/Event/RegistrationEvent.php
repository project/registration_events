<?php

namespace Drupal\registration_events\Event;

use Drupal\user\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class RegistrationEvent extends Event {

    /**
     * @var string
     */
    public $registrationKey;

    /**
     * @var mixed
     */
    public $registrationData;

    /**
     * @var User
     */
    public $account;

    /**
     * RegistrationEvent constructor.
     * @param string $registrationKey
     * @param $registrationData
     * @param User $account
     */
    public function __construct(string $registrationKey, $registrationData, User $account) {
        $this->registrationKey = $registrationKey;
        $this->registrationData = $registrationData;
        $this->account = $account;
    }

    /**
     * @return string
     */
    public function getRegistrationKey(): string
    {
        return $this->registrationKey;
    }

    /**
     * @param string $registrationKey
     */
    public function setRegistrationKey(string $registrationKey): void
    {
        $this->registrationKey = $registrationKey;
    }

    /**
     * @return mixed
     */
    public function getRegistrationData()
    {
        return $this->registrationData;
    }

    /**
     * @param mixed $registrationData
     */
    public function setRegistrationData($registrationData): void
    {
        $this->registrationData = $registrationData;
    }

    /**
     * @return User
     */
    public function getAccount(): User
    {
        return $this->account;
    }

    /**
     * @param User $account
     */
    public function setAccount(User $account): void
    {
        $this->account = $account;
    }
}
